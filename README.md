# Actually README #


# So, what features were added? #

### Non-Scripting: ###
* Level
* Collectables
* New UI elements - Lives, Count Text, Ghost Count
* New win/lose text screens
* A death field
* Melee range to John Lemon 
* Setting up Sword mesh on player model
* Some audio for game objects
(Creative license CC0 from freesound.org every time)

### Scripting: ###
* Player life count
* Player death upon falling (currently impossible, for future use)
* Player fail state 
* A button to reset to initial condition upon win or death 
* Projectiles
* Inter-locking Scripts

To win kill 13 all ghosts or collect and save all 8 John Lemons.

Falling off the ledge or losing too many lives causes you to lose, but you can reset. 

A lot of commented out code from following class examples, any reasoning need will be listed here otherwise.
