﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Collectable : MonoBehaviour
{
    public int count2;

    public AudioSource m_AudioSource1;

    public TextMeshProUGUI collectableText;

    public GameObject Player;

    public GameObject winTextObject;

    public GameObject buttonTextObject;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("JohnLemon"))
        {
            print(other.gameObject.name);
            PlayerMovement player_m = other.transform.parent.GetComponent<PlayerMovement>();
            player_m.count++;
            m_AudioSource1.Play();
            player_m.SetCollectableText();
            gameObject.SetActive(false);
        }
    }
}