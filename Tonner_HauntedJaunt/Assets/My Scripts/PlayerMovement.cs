﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 40f;

    public int ghostCount;

    public int lives;

    Animator m_Animator;

    Vector3 m_Movement;

    GameObject enemyToBonk;

    public Rigidbody m_Rigidbody;

    AudioSource m_AudioSource; // Declaring an audio source so the script can play it

    public AudioSource m_AudioSource2;

    public AudioSource m_AudioSource3;

    public AudioSource m_AudioSource4;

    public AudioSource m_AudioSource5;

    public AudioSource m_AudioSource6;

    Quaternion m_Rotation = Quaternion.identity;

    // public bool invulnerable;

    // float invulnerableCooldown;

    // public Renderer renderer;

    // public Material matJohnPBR;

    // public Material matJohnToon;

    // public Material matJohnTransparent;

    // Maybe make invisibility into a collectable to make it not subtract from the player experience. 

    //  public WaypointPatrol[] allPatrols; // Can talk to a collection of objects at a time through an array

    public GameObject projectilePrefab;
    
    public Transform shotSpawn;

    public float shotSpeed = 10f;

    public TextMeshProUGUI lifeText;

    public GameObject winTextObject;

    public GameObject loseTextObject;

    public Button buttonTextObject;

    public GameObject buttonTextObject2;

    public GameObject selfPlayer;

    public TextMeshProUGUI GhostCounterText;

    public TextMeshProUGUI collectableText;

    public GameObject AudioListener;

    //Collectable References

    public GameObject collectable1;
    public GameObject collectable2;
    public GameObject collectable3;
    public GameObject collectable4;
    public GameObject collectable5;
    public GameObject collectable6;
    public GameObject collectable7;
    public GameObject collectable8;


    // Counts

    public int count2;

    public int count;

    // float jumpTimeLeft = 0f;

    public Slider slider;

    void Start()
    {
        selfPlayer.SetActive(true);
        buttonTextObject.onClick.AddListener(TaskOnClick);
        AudioListener.SetActive(false);
        collectable1.SetActive(true);
        collectable2.SetActive(true);
        collectable3.SetActive(true);
        collectable4.SetActive(true);
        collectable5.SetActive(true);
        collectable6.SetActive(true);
        collectable7.SetActive(true);
        collectable8.SetActive(true);

        m_AudioSource = GetComponent<AudioSource>(); // The system will retrive the right component at the start of play

        m_Animator = GetComponent<Animator>(); // This is so the script can reference the animator component at start

        // The script needs the rigid body component as well to be referenced in script at start

        //allPatrols = FindObjectsOfType<WaypointPatrol>();

        lives = 7;
        ghostCount = 0;
        count = 0;

        SetLivesText();

        SetCollectableText();

        SetGhostCount();

        winTextObject.SetActive(false); 
        loseTextObject.SetActive(false); 
        buttonTextObject2.SetActive(false);

    }


    /* private void Update()
     {
         if (invulnerable)
         {
             invulnerableCooldown -= Time.deltaTime;
             if (invulnerableCooldown <= 0f)
             {
                 invulnerable = false;
             }
             Material[] mats = new Materials[]{matJohnTransparent, matJohnTransparent}
             renderer.materials = mats        

         }
         if (Input.GetKeyDown(KeyCode.Space) && invulnerableCooldown <= 0) // Did the player hit space bar or not? And has the cooldown exhausted
         {
             invulnerable = true;
             invulnerableCooldown = 5f;

             Material[] mats = new Materials[]{matJohnTransparent, matJohnTransparent}
             renderer.materials = mats

         }
     }
    */

    /*  private void Update()
      {
          if (Input.GetKeyDown(KeyCode.Space))
          {
              FreezeGhosts();
          }
      }

      void FreezeGhosts()
      {
          foreach (WaypointPatrol patrol in allPatrols)
          {
              if (patrol != null)
              {
                  patrol.navMeshAgent.isStopped = true;
              }
          }

          CancelInvoke("EndFreeze");
          Invoke("EndFreeze", 2f);
      }

      void EndFreeze()
      {
          foreach (WaypointPatrol patrol in allPatrols)
          {
              if (patrol != null)
              {
                  patrol.navMeshAgent.isStopped = false;
              }
          }
      }

      */

    private void Update()
    {
       // if (jumpTimeLeft >= 0)
          //  jumpTimeLeft -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);
            m_AudioSource3.Play();
            Rigidbody projectileRB = projectile.GetComponent<Rigidbody>();
            projectileRB.velocity = transform.forward * shotSpeed; // Shoots a transform forward from Z-axis with a velocity vector transform.right - x transform.up - Y
        }

        if (Input.GetKeyDown(KeyCode.Space) && enemyToBonk != null)
        {
            m_AudioSource2.Play();
            ghostCount++;
            enemyToBonk.GetComponent<EnemyHealth2>().TakeDamage(1);
        }

        //if (Input.GetKeyDown(KeyCode.LeftShift))
        // {
        //     m_Rigidbody.AddForce(Vector3.up * 300f);
        //jumpTimeLeft = 1.5f;
        //  }
        // if (Input.GetKeyDown(KeyCode.Space))
        // {
        //  if(count > 0)
        // {
        //    count--;
        //   SetSlider();
        // }
        // }
    }

    public void SetAudioListener()
    {
        AudioListener.SetActive(true);
    }

    void SetSlider()
    {
        slider.value = count;
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");

        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);

        m_Movement.Normalize();


        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);

        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        bool isWalking = hasHorizontalInput || hasVerticalInput;

        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking) // System checks to see if John Lemon is walking
        {
            if (!m_AudioSource.isPlaying) // Checks to see if the audio is playing, if it is, then it does nothing. 
            {
                m_AudioSource.Play(); // This will play the audio if it isn't already playing
            }
        }
        else
        {
            m_AudioSource.Stop(); // If John Lemon isn't walking then it will stop the audio 
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);

        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);

        m_Rigidbody.MoveRotation(m_Rotation);
    }

    void OnTriggerEnter(Collider other) {
        
        if (other.gameObject.CompareTag("Death"))
        {
            transform.localPosition = new Vector3(-9.8f, 0, -2.979f);
            lives -= 10;

            SetLivesText();

            m_AudioSource6.Play();
            m_Rigidbody.angularVelocity = Vector3.zero;
            m_Rigidbody.velocity = Vector3.zero;
        }
        if (other.CompareTag("Ghost"))
        {
            enemyToBonk = other.gameObject;
        }
    }

    void TaskOnClick()
    {
        m_Rigidbody.angularVelocity = Vector3.zero;
        m_Rigidbody.velocity = Vector3.zero;

        transform.localPosition = new Vector3(-9.8f, 0, -3.2f);

        Start();
    }

    public void SetLivesText()
    {
        lifeText.text = "Lives: " + lives.ToString();
        if (lives <= 0)
        {
            loseTextObject.SetActive(true);
            buttonTextObject2.SetActive(true);
            m_AudioSource5.Play();
            selfPlayer.SetActive(false);
        }
    }

    public void SetGhostCount() 
    {
        GhostCounterText.text = "Ghosts Dead: " + ghostCount.ToString();

        if (ghostCount >= 13)
        {
            winTextObject.SetActive(true);
            buttonTextObject2.SetActive(true);
            SetAudioListener();
            m_AudioSource5.Play();
            selfPlayer.SetActive(false);
        }
    }

    public void SetCollectableText()
    {
        collectableText.text = "Lemons Saved: " + count.ToString() + "/8";

        if (count >= 8)
        {
            winTextObject.SetActive(true);
            buttonTextObject2.SetActive(true);
            SetAudioListener();
            m_AudioSource5.Play();
            selfPlayer.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ghost"))
        {
            if (enemyToBonk == other.gameObject)
            {
                enemyToBonk = null;
            }
        }
    }
}



