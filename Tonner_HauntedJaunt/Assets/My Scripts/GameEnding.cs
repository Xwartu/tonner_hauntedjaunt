﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Need to releod the scene and thus that requires more functionality than a base script
// So we add in a new class, for this time it's SceneManagment 

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f; // Static value for fade Timer Amount

    public float displayImageDuration = 1f; // Static value since we want the image to be seen before quitting

    public GameObject player; // Here to reference the player for the trigger 

    public CanvasGroup exitBackgroundImageCanvasGroup; // Need this referenced so we can change the alpha values with update

    public AudioSource exitAudio; // Adding audio to play after the game finishes

    public CanvasGroup caughtBackgroundImageCanvasGroup; // Now we need this to trigger the failed game state fade

    public AudioSource caughtAudio; // Adding audio to play after the player gets caught

    bool m_IsPlayerAtExit; // Need to know when the player reaches the exit to activate the rest of the code

    bool m_IsPlayerCaught; // Using this variable to know if the player has been caught

    float m_Timer; // Timer for allowing the fade in before closing out the game. 

    bool m_HasAudioPlayed; // System needs to know if the audio has played yet for it's corresponding part

    // public float timeLeft = 3;

    void OnTriggerEnter(Collider other) //Where the event for the trigger will be called upon collison
    {
        if (other.gameObject == player) // Using the equivalency operator to connect the trigger to the player
        {
            m_IsPlayerAtExit = true; // Setting the condtional to true so that update knows the player is there. 
        }
    }

    public void CaughtPlayer() // Setting up a public function so John Lemon's caught state can be updated outside the script
    {
        m_IsPlayerCaught = true; // When called it will set the bool to true. 
    }

    void Update() //We need to change the alpha every frame for this to be effective, as well as check what's happened to John Lemon. 
    {
        if (m_IsPlayerAtExit) // Using the prviously checked trigger as a conditional.
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio); // When the player is at the exit we need to call the process to end the level
            //Not restarting the level since it's a win condtion
        }
        else if (m_IsPlayerCaught) // Using else-if to check past the first condtional for a potential game ending event each frame. 
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio); // When the player is caught the level needs to end as well
                                                                           //Resetting the level since this is a failure state 

           /* timeLeft -= Time.deltaTime;
            print("timeLeft: " + timeLeft);
            if (timeLeft <= 0) ;
            {
                EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
            } */

        }
    }
    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource) // The function where we appropiately start the process to end the level
    {
        if (!m_HasAudioPlayed) // the != makes sure to check and see if HasAudioPlayed is false
        {
            audioSource.Play(); // If it hasn't, we play the audio
            m_HasAudioPlayed = true; // Then set it to true so it won't play again 
        }

        m_Timer += Time.deltaTime; // Need to start the timer for it to function

        imageCanvasGroup.alpha = m_Timer / fadeDuration; // Re-using values in game to get differing alpha values to change over time
        //Specficifying which fade element we want changed when end level is called

        if (m_Timer > fadeDuration + displayImageDuration) // When this occurs we finally quit out of the game
            // Adding displayImageduration makes it so it takes longer for the game to genuinely quit out. 
        {
            if (doRestart) //Checking if we need to restart the level
            {
                SceneManager.LoadScene(0); // Will use this inherent function to restart the scene to a base state upon failure.
                // A static method does not require a 
            }
            else // If we do not need to restart based off the bool, then we quit the game. 
            {
                Application.Quit(); // Using this built-in function we quit out of the game when it's called
                // Will only happen after the timer exceeds the fade duratio
            }
        }
    }
}
