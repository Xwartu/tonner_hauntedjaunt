﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMelee : MonoBehaviour
{
    public AudioSource m_AudioSource;

    GameObject enemyToBonk;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.KeypadEnter) && enemyToBonk != null){
            m_AudioSource.Play();
            enemyToBonk.GetComponent<EnemyHealth2>().TakeDamage(1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ghost"))
        {
            enemyToBonk = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ghost")){ 
            if (enemyToBonk == other.gameObject)
            {
                enemyToBonk = null;
            }
        }
    }
}
