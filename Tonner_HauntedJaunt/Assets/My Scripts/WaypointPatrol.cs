﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; // We are getting more complicated in the movement of the enemy so we need to reference the NavMeshAgent Class

public class WaypointPatrol : MonoBehaviour
{

    public NavMeshAgent navMeshAgent; // Referencing the Navmesh so the ghost knows what to follow in script

    public Transform[] waypoints; // An array created to hold all of our waypoints referenced through code

    int m_CurrentWaypointIndex; // A variable so we the system knows which waypoint to track from the index

    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position); // Add a waypoint at the starting location so that the ghost returns to where they start
    }

    void Update() // Need to check every frame, hence Update 
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) // We need to check and see if the ghost has reached the new waypoint. 
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length; // This is used to update the current index and set the NavMeshAgent's destination 
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position); // This now references the ghost's current location to the new waypoint, using indexes as increments.
        }
    }
}
