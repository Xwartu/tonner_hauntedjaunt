﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth2 : MonoBehaviour
{
    public GameObject player;

    public int health = 1;

    public AudioSource m_AudioSource;

    public AudioSource m_AudioSource2;

    public void TakeDamage (int damageAmount)
    {
        health -= damageAmount;

        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("JohnLemon"))
        {
            PlayerMovement player_m = other.transform.parent.GetComponent<PlayerMovement>();
            player_m.lives--;
            m_AudioSource.Play();
            player_m.SetLivesText();
        }
        if (other.gameObject.CompareTag("Projectile"))
        {
            PlayerMovement m_player = player.GetComponent<PlayerMovement>();
            m_player.ghostCount++;
            m_AudioSource2.Play();
            m_player.SetGhostCount();
        }
    }
}
