﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public Transform player; //Adding in the player as a variable so it can detect it

    public GameEnding gameEnding; // Need a variable to trigger the end state upon losing 
    //Will likely need to change this for Sir John Lemon

    bool m_IsPlayerInRange; // Adding in a conditional to trigger game behaviors for detection

    void OnTriggerEnter(Collider other) // The fucntion for detecting collisions 
    {
        if (other.transform == player) // Setting up an equivalency operator so the game knows we want to refer only to the player.
        {
            m_IsPlayerInRange = true; // We want to know if John Lemon is in range to trigger the behavior
        }
    }
    void OnTriggerExit(Collider other) // Just being in the trigger isn't an instant game over, so we want to know if the playerexits it as well
    {
        //Same reasoning as before but flipped
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update() //Checking each frame to see if the player is in the line of sight
    {
        if (m_IsPlayerInRange) // We should only check when the player actually is in range to save resources
        {
            Vector3 direction = player.position - transform.position + Vector3.up; // Setting up the direction for our ray
            Ray ray = new Ray(transform.position, direction); // Actually instantiating our ray with a constrcutor "new"
            RaycastHit raycastHit; // Specfically collecting data from what the ray collides with

            if (Physics.Raycast(ray, out raycastHit)) // Now using the inherent aspects of the ray we can check for the player
            //This also is what uses and perfoms our raycast
            {
                if (raycastHit.collider.transform == player) // Now if the ray specficially casts/hits upon the player
                {
                    // PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();

                    // if (!playerMovement.invulnerable)
                    {
                        gameEnding.CaughtPlayer(); // Refers to the game ending script to trigger the failure state since the player was caught. 

                    }
                }
            }
        }
    }
}
