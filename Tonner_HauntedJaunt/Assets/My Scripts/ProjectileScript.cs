﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (!other.isTrigger) 
        { 
            if (other.gameObject.CompareTag("Ghost"))
            {
                EnemyHealth2 eHealth = other.gameObject.GetComponent<EnemyHealth2>();

                if (eHealth != null)
                {
                    eHealth.TakeDamage(1);
                }
            }
            Destroy(gameObject);
            
        }
    }
}
